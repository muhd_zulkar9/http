from sqlite3 import Error
import os
import sqlite3


class Database(object):
    conn = None

    def __init__(self):
        self.conn = sqlite3.connect('async.db', check_same_thread=False)
        self.cur = self.conn.cursor()

    def create_table(self):
        self.cur.execute(""" CREATE TABLE IF NOT EXISTS async_table(
                        "id" INTEGER PRIMARY KEY,   
                        "url" TEXT,
                        "status" BLOB,
                        "timed" BLOB
                        );""")
        self.conn.commit()

    def insert(self, csv_reader):
        self.cur = self.conn.cursor()
        sql = """INSERT INTO async_table(url)
        VALUES(:url)"""
        self.cur.executemany(sql, csv_reader)
        self.conn.commit()

    def insert_data(self, url):
        self.cur = self.conn.cursor()
        self.cur.execute(
            'INSERT INTO async_table(url) VALUES(?)', (url))
        self.conn.commit()

    def view(self):
        self.cur = self.conn.cursor()
        self.conn.row_factory = sqlite3.Row
        self.cur.execute("SELECT * from async_table")
        rows = self.cur.fetchall()
        self.conn.commit()
        return rows

    def delete(self, id):
        self.cur = self.conn.cursor()
        self.cur.execute("DELETE from async_table WHERE id = ?", (id,))
        self.conn.commit()

    def select(self, id):
        self.cur = self.conn.cursor()
        self.conn.row_factory = sqlite3.Row
        self.cur.execute("SELECT * from async_table WHERE id = ?", (id,))
        rows = self.cur.fetchall()
        self.conn.commit()
        return rows

    def select_one(self, id):
        self.cur = self.conn.cursor()
        self.conn.row_factory = sqlite3.Row
        self.cur.execute("SELECT * from async_table WHERE id = ?", (id,))
        rows = self.cur.fetchone()
        self.conn.commit()
        return rows

    def update(self, id, status, timed):
        self.cur = self.conn.cursor()
        self.cur.execute("""Update async_table
                    SET
                    id = ?,
                    status = ?,
                    timed = ?
                    WHERE id = ?""", (id, status, timed, id))
        self.conn.commit()

    def test_update(self, id, url, status, timed):
        self.cur = self.conn.cursor()
        self.cur.execute("""Update async_table
                    SET
                    id = ?,
                    url = ?,
                    status = ?,
                    timed = ?
                    WHERE id = ?""", (id, url, status, timed,  id))
        self.conn.commit()

    def update_form(self, id, url):
        self.cur = self.conn.cursor()
        self.cur.execute("""Update async_table
                    SET
                    id = ?,
                    url = ?
                    WHERE id = ?""", (id, url, id))
        self.conn.commit()

    # def drop_table:
    #     self

    def disconnect(self):
        self.conn.close()
