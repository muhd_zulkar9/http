from flask import Flask, make_response, render_template, request, redirect
import csv
import sqlite3
import database
from sqlite3 import Error
from database import Database
from requests.exceptions import HTTPError
import requests as req
import asyncio
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime


app = Flask(__name__)
db = Database()
db.create_table()


@app.route('/', methods=['GET'])
@app.route('/home', methods=['GET'])
def home():
    db.view()
    row = db.view()

    return render_template('home.html', row=row)


@app.route('/delete', methods=['POST'])
def delete():
    id = request.form.get('id')
    db.delete(id)

    return redirect('/')


# @app.route('/gettext', methods=['GET'])
# def gettext():
#     url = request.form.get('url')
#     result = []
#     response = req.get(url)
#     result.append(response.text)

#     return render_template('text.html', result=result)


@app.route('/insertfile', methods=['GET'])
def insertfile():
    return render_template('insert.html')


@app.route('/save', methods=['POST'])
def save():
    user_csv = request.form.get('data_file')
    with open(user_csv, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        db.insert(csv_reader)
        main()

        return redirect('/')


@app.route('/update', methods=['POST'])
def update():
    id = request.form.get('id')
    db.select(id)
    rows = db.select(id)
    return render_template('update.html', rows=rows)


@app.route('/saveupdate', methods=['POST'])
def saveupdate():
    id = request.form.get('id')
    url = request.form.get('url')

    db.update_form(id, url)

    return redirect('/')


@app.route('/refreshall', methods=['GET'])
def refreshall():
    main()
    return redirect('/')


@app.route('/refresh', methods=['POST'])
def refresh():
    now = datetime.now()
    timed = now.strftime("%m/%d/%Y, %H:%M:%S")
    id = request.form.get('id')
    db.select_one(id)
    rows = db.select_one(id)

    response = req.get(rows['url'])
    status = response.status_code
    db.update(id, status, timed)
    return redirect('/')


@app.route('/addpage', methods=['GET'])
def addpage():
    return render_template('add.html')


@app.route('/add', methods=['POST'])
def add():
    url = request.form.get('url')
    db.insert_data(url)

    return redirect('/')


def fetch(session, url, id):
    with session.get(url) as response:
        now = datetime.now()
        timed = now.strftime("%m/%d/%Y, %H:%M:%S")
        data = response.text
        status = response.status_code

        db.test_update(id, url, status, timed)

        return data


async def get_data_asynchronous():
    db.view()
    row = db.view()
    for line in row:
        with ThreadPoolExecutor(max_workers=20) as executor:
            with req.Session() as session:
                loop = asyncio.get_event_loop()
                tasks = [
                    loop.run_in_executor(
                        executor,
                        fetch,
                        *(session, line['url'], line['id'])
                    )
                    for url in line['url']
                ]
    else:
        pass


def main():
    asyncio.run(get_data_asynchronous())


if __name__ == '__main__':
    app.run(debug=True)
